module gitlab.com/digitaldollar/bifrost/dogscript

go 1.14

require (
	github.com/davecgh/go-spew v1.1.1
	github.com/eager7/dogd v0.0.0-20200427085516-2caf59f59dbb
	github.com/eager7/doglog v0.0.0-20200427040431-a0db59f0a792
	github.com/eager7/dogutil v0.0.0-20200427040807-200e961ba4b5
	golang.org/x/crypto v0.0.0-20201221181555-eec23a3978ad
)
